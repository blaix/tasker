{ pkgs ? import <nixpkgs> {} 
, stdenv ? pkgs.stdenv
, lib ? pkgs.lib
, bashInteractive ? pkgs.bashInteractive
, dateutils ? pkgs.dateutils
, glow ? pkgs.glow
, fd ? pkgs.fd
, fzf ? pkgs.fzf
, ripgrep ? pkgs.ripgrep
, findutils ? pkgs.findutils
, makeWrapper ? pkgs.makeWrapper
}: let
  inherit (lib) makeBinPath;
  taskfiles = import (builtins.fetchGit {
    url = "https://codeberg.org/blaix/taskfiles.git";
    rev = "0989a0c5c1305142997fb7b89664d5b64d467fd1";
  });
in
# We use `rec` here so that we can reuse the `buildInputs` variable.
stdenv.mkDerivation rec {
  name = "tasker";
  version = "0.0.1";
  src = ./.;
  dontUnpack = true;
  nativeBuildInputs = [ makeWrapper ];
  buildInputs = [
    bashInteractive # required for readline support (bash -i)
    dateutils       # for date math (e.g. get last monday for tasker weekly)
    glow            # markdown viewer: https://github.com/charmbracelet/glow
    fzf             # fuzzy finder: https://github.com/junegunn/fzf
    fd              # better find: https://github.com/sharkdp/fd
    ripgrep         # better grep: https://github.com/BurntSushi/ripgrep
    (pkgs.callPackage taskfiles {})
  ];
  installPhase = ''
    mkdir -p $out/bin
    cp '${./tasker.sh}' $out/bin/tasker
    substituteInPlace $out/bin/tasker \
      --replace "glow" ${glow}/bin/glow \
      --replace "fd" ${fd}/bin/fd \
      --replace "fzf" ${fzf}/bin/fzf \
      --replace "rg " "${ripgrep}/bin/rg " \
      --replace "xargs" ${findutils}/bin/xargs \
      --replace "dateround" ${dateutils}/bin/dateround \
      --replace "dateadd" ${dateutils}/bin/dateadd
    wrapProgram $out/bin/tasker \
      --prefix PATH : '${makeBinPath buildInputs}'
  '';
}
