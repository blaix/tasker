#!/usr/bin/env bash

############################################################
# Config                                                   #
############################################################

set -e

if [[ -z "$TASKER_HOME" ]]; then
  echo "TASKER_HOME must be set" 1>&2
  exit 1
fi

set -u

today=$(date +%Y-%m-%d)
date_token="%%DATE%%"

############################################################
# Colors                                                   #
############################################################

print() {
  echo -e "$1"
}

red="\033[0;31m"
green="\033[0;32m"
#purple="\033[0;35m"
cyan="\033[0;36m"
yellow="\033[0;33m"
white="\033[1;37m"
nc="\033[0m" # No Color

############################################################
# Help                                                     #
############################################################

Help(){
  print "${green}tasker:${nc} cli to manage tasks."
  print ""
  print "  A task is any file ending in .md in the \$TASKER_HOME dir"
  print "  excluding files and directories starting with '_' or '.'"
  print "  Files can contain special prioritization flags on their own line:"
  print ""
  print "    ${cyan}%p-[0-9]${nc}      - priority"
  print "    ${cyan}%d-YYYY-MM-DD${nc} - due: when the task is literally due."
  print "    ${cyan}%s-YYYY-MM-DD${nc} - scheduled: when you want to work on the task."
  print "    ${cyan}%a-YYYY-MM-DD${nc} - available: when the task becomes available."
  print ""
  print "  Commands that list tasks will sort them using those tags,"
  print "  prioritizing them in the above order, and excluding tasks"
  print "  with an available date in the future."
  print ""
  print "${yellow}USAGE:${nc}"
  print ""
  print "  ${cyan}tasker${nc} [OPTIONS] [SUBCOMMAND]"
  print ""
  print "${yellow}OPTIONS:${nc}"
  print ""
  print "  ${cyan}-h, --help${nc}  Print this help information"
  print ""
  print "${yellow}SUBCOMMANDS:${nc}"
  print ""
  print "  ${cyan}tasks|<empty>${nc}"
  print "    (Default behavior) Choose from tasks ordered by"
  print "    priority, then due, then scheduled, then available."
  print ""
  print "  ${cyan}new|open${nc}"
  print "    Create or open task file."
  print "    Offers fuzzy-finder for directory and then prompts for filename."
  print "    Copies ${white}_template.md${nc} if exists in chosen directory."
  print "    Any instances of ${date_token} will be replaced with today's date"
  print "    in the YYYY-MM-DD format that tasker recognizes. So you can use"
  print "    templates with the daily/weekly/etc commands for repeating tasks."
  print ""
  print "  ${cyan}done [/path/to/task.md]${nc}"
  print "    Complete a task."
  print "    Prompts to choose task if one is not specified."
  print "    Moves task to a _done folder in the parent directory"
  print "    where it will be excluded from normal searches."
  print ""
  print "  ${cyan}inbox${nc}"
  print "    Add a task to $TASKER_HOME/inbox"
  print "    Creates directory if it doesn't exist."
  print ""
  print "  ${cyan}daily${nc}"
  print "    Open or create a task file with"
  print "    today's date as the filename."
  print ""
  print "  ${cyan}tomorrow${nc}"
  print "    Open or create a task file with"
  print "    tomorrow's date as the filename."
  print ""
  print "  ${cyan}weekly${nc}"
  print "    Open or create a task file with"
  print "    last monday's date as the filename."
  print ""
  print "  ${cyan}monthly${nc}"
  print "    Open or create a task file with"
  print "    the first of this month as the filename."
  print ""
  print "  ${cyan}yearly${nc}"
  print "    Open or create a task file with"
  print "    the first of this year as the filename."
  print ""
  print "  ${cyan}rand${nc}"
  print "    Choose a directory and you will be presented"
  print "    with a short, random list of tasks from that"
  print "    directory (recursive) to choose from."
  print ""
  print "  ${cyan}view /path/to/task.md${nc}"
  print "    View contents of task file"
  print "    prepended with priorities and dates."
  print ""
  print "  ${cyan}home${nc}"
  print "    Open \$EDITOR in \$TASKER_HOME."
  print ""
  print "  ${cyan}rm${nc}"
  print "    Choose a task to delete."
  print ""
  print "${yellow}CONFIG:${nc}"
  print ""
  print "  ${cyan}TASKER_HOME${nc}"
  print ""
  print "    tasker expects this environment variable to point"
  print "    to the root directory holding all of your tasks."
}

############################################################
# Task functions
############################################################

ChooseDirectory(){
  if [[ -z ${1-""} ]]; then
    label="Choose directory"
  else
    label="Choose directory for $1"
  fi
  choice=$(fd . "$TASKER_HOME" --type d | rg "^${TASKER_HOME}/(.+)$" -r '$1' \
    | fzf --ansi --layout=reverse --info=hidden \
      --border --border-label "$label" \
      --border-label-pos "5:top" \
      --preview "echo {} | sed -e 's|^|$TASKER_HOME/|' | xargs -I % ls --color=always %")
  echo "${TASKER_HOME}/${choice}"
}

ChooseTask(){
  taskfiles | \
    rg -o "${TASKER_HOME}/(.+)\.md$" -r '$1' | \
    fzf --ansi --tiebreak=index --keep-right --layout=reverse --info=hidden \
      --border --border-label "Type or use arrow keys to choose task" \
      --border-label-pos "5:top" \
      --preview "$0 'view' {}.md"
}

ViewTask(){
  file="$1"
  if [[ ! -f "$file" ]]; then
    file="${TASKER_HOME}/${file}"
  fi
  if [[ ! -f "$file" ]]; then
    echo -e "${red}Unrecognized file: ${1}${nc}"
    exit 1
  fi
  p="$(rg "^\s*%p-([0-9]{1})\s*$" -r '$1' "$file" || echo "")"
  d="$(rg "^%d-([0-9]{4}-[0-9]{2}-[0-9]{2})\s*$" -r '$1' "$file" || echo "")"
  s="$(rg "^%s-([0-9]{4}-[0-9]{2}-[0-9]{2})\s*$" -r '$1' "$file" || echo "")"
  a="$(rg "^%a-([0-9]{4}-[0-9]{2}-[0-9]{2})\s*$" -r '$1' "$file" || echo "")"
  echo -e "${cyan}┏━━━━━━━━━━━━━━━━━━━━━━━┓"
  echo -e "Priority:${p}\nDue:${d}\nScheduled:${s}\nAvailable:${a}" | \
    awk -F ':' '{ printf "┃%10s: %-10s ┃\n", $1, $2 }'
  echo -e "┗━━━━━━━━━━━━━━━━━━━━━━━┛${nc}"
  glow --style=auto "$file"
}

CompleteTask(){
  task="$1"
  task=$(echo "$task" | rg "^(.+?)(\.md)?$" -r '$1.md')
  echo -e "\n\n---\nCompleted: $(date)\n" >> "$task"
  dest="$(dirname "$task")/_done/"
  mkdir -p "$dest"
  mv "$task" "$dest" 
  echo "Task moved to: $dest$fname"
}

############################################################
# Handle subcommands
############################################################

fname=""
dname=""
subcommand=${1:-""}

case $subcommand in

  "-h" | "--help")
    Help
    exit 0
    ;;

  "" | "tasks")
    choice=$(ChooseTask)
    fname="$(basename "$choice").md"
    dname="${TASKER_HOME}/$(dirname "$choice")/"
    ;;

  "rm")
    choice=$(ChooseTask)
    rm -i "${TASKER_HOME}/${choice}.md"
    exit 0
    ;;

  "done")
    task=${2:-""}
    if [[ -z $task ]]; then
      task="${TASKER_HOME}/$(ChooseTask)"
    fi
    CompleteTask "$task"
    exit 0
    ;;

  "new" | "open")
    ;;

  "r" | "rand")
    directory=$(ChooseDirectory)
    choice="${TASKER_HOME}/$(ChooseTask "rand" "$directory")"
    dname=$(dirname "$choice")
    fname="$(basename "$choice").md"
    ;;

  "i" | "inbox")
    dname="${TASKER_HOME}/inbox/"
    mkdir -p "$dname"
    ;;

  "d" | "daily")
    fname="$(date +%Y-%m-%d).md"
    ;;

  "t" | "tomorrow")
    today=$(dateadd today +1d)
    fname="${today}.md"
    ;;

  "w" | "weekly")
    fname="$(dateround today -- -Mon).md"
    ;;

  "m" | "monthly")
    fname="$(date +%Y-%m-01).md"
    ;;

  "y" | "yearly")
    fname="$(date +%Y-01-01).md"
    ;;

  "v" | "view")
    if [[ -z "${2-""}" ]]; then
      print "${red}You must provide path to task file.${nc}"
      exit 1
    fi
    ViewTask "$2"
    exit 0
    ;;

  "home")
    cd "$TASKER_HOME"
    "$EDITOR"
    exit 0
    ;;

  *)
    print "${red}Unrecognized subcommand."
    print "Run tasker --help for usage.${nc}"
    exit 2
    ;;
esac

############################################################
# Get task file                
############################################################

if [[ -z $dname ]]; then
  dname=$(ChooseDirectory "$fname")
fi

if [[ -z $fname ]]; then
  tree "$dname"
  read -re -i "$dname" -p "Filename: " input
  # ensure .md extension
  input=$(echo "$input" | rg "^(.+?)(\.md)?$" -r '$1.md')
  fname=$(basename "$input")
  # reset dname in case they change the path
  dname=$(dirname "$input") 
  # allow creation of new directories
  mkdir -p "$dname"
fi

file="$(echo "${dname}/${fname}" | sed -e 's|//|/|g')"

# Copy template if exists                                  #
template="${dname}/_template.md"
if [[ ! -f "$file" ]]; then
  if [[ -f "$template" ]]; then
    cp "$template" "$file"
    sed -e "s/${date_token}/${today}/g" -i "" "$file"
  else
    touch "$file"
  fi
fi

############################################################
# Decide what to do with the task file
############################################################

OpenAtLastLineIfPossible(){
  if [[ "$EDITOR" =~ ^n?vim(\s|$) ]]; then
    "$EDITOR" "+normal G<cr>" "$1"
  else
    "$EDITOR" "$1"
  fi
}

choice=$(echo -e "Edit\nComplete\nComplete and Edit\nView\nDelete\nExit" \
    | fzf --ansi --layout reverse --info hidden \
        --border --border-label "$file" \
        --border-label-pos "5:top" \
        --preview "$0 'view' \"$file\"")

case "$choice" in

  "Edit")
    cd "$TASKER_HOME"
    "$EDITOR" "$file"
    exit 0
    ;;

  "Complete")
    "$0" "done" "$file"
    exit 0
    ;;

  "Complete and Edit")
    "$0" "done" "$file"
    OpenAtLastLineIfPossible "${dname}/_done/${fname}"
    exit 0
    ;;

  "View")
    "$0" "view" "$file"
    exit 0
    ;;

  "Delete")
    rm -i "$file"
    exit 0
    ;;

  "Exit")
    exit 0
    ;;

  *)
    echo -e "Unrecognized command."
    exit 1
    ;;

esac
