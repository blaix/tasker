with import <nixpkgs> {};
mkShell {
  buildInputs = [
    bashInteractive
    dateutils
    fd
    glow
    ripgrep
    shellcheck
  ];
}

