* [X] Tasks in `_done` are not being filtered out!
* [X] Completing tasks from within tasker
* [ ] Desktop widget(s) with [uebersicht](http://tracesof.net/uebersicht/) !!
* [ ] Do I need a way to spawn multiple tasks from a template?
      E.g. I want monthly bills prioritized as p-1 due,
           but the trigger for opening that is in my daily or weekly template,
           which I may not want p-1 due...
* [ ] New name? there's an android app named tasker
* [ ] Use from within vim?
  * ToggleTerm looks promising for this! https://github.com/akinsho/toggleterm.nvim
  * As a telescope plugin?
  * Use server? E.g.:
    nvim --listen ./nvim-server.pipe # <= open vim and listen for commands
    ./tasker.sh tasks | xargs nvim --server ./nvim-server.pipe --remote # <= open task in the above instance
* [ ] Non-markdown files:
  * [ ] Full blown support: just don't try to parse tags?
  * [ ] Subcommand to view/choose from list of them?
  * [ ] If they are in inbox, consider them a pending task?
    * For dropping screenshots, etc. during meetings
* [ ] Warn/prevent badly-formatted tags somehow?
  * E.g. lost some time debugging why "%a-2023-01-1" wasn't being captured
* [ ] README & Docs
  * [ ] Note how to try it out with nix:
        nix-shell -p 'callPackage (fetchGit https://codeberg.org/blaix/tasker.git) {}'
* [ ] Installable packages:
  * [ ] homebrew (tap blaix?)
  * [ ] npm?
  * [ ] others?
* [ ] Listing tasks is painfully slow...
      * Consider a rewrite in go using the charm libraries:
        * https://github.com/charmbracelet/bubbletea (uses the elm architecture!)
        * https://github.com/charmbracelet/bubbles
          * The Table component in particular looks like a perfect fit:
            https://github.com/charmbracelet/bubbles#table
        * https://github.com/charmbracelet/lipgloss
        * https://github.com/charmbracelet/glamour
      * Could maybe use charm's kv store or other tools,
        even if not going full blown TUI: https://github.com/charmbracelet/charm

